class Ligne:
	
	def __init__(self, parent, index):
		self.parent = parent
		self.index = index
	
	def __len__(self):
		return self.parent.largeur
	
	def __getitem__(self, key):
		return self.parent[self.index, key]
		
	def __setitem__(self, key, value):
		self.parent[self.index, key] = value
		
	def __repr__(self):
		return str(self.parent.lignes[self.index])
		
	def echanger(self, ligne):
		ligne1, ligne2 = self.index, ligne
		
		for i in range(self.parent.largeur):
			self.parent.raw[ligne1*self.parent.largeur+i], self.parent.raw[ligne2*self.parent.largeur+i] = self.parent.raw[ligne2*self.parent.largeur+i], self.parent.raw[ligne1*self.parent.largeur+i]
		# On signale que deux changements ont été effectués
		self.parent.dirty.append((self.parent.LIGNE, ligne1))
		self.parent.dirty.append((self.parent.LIGNE, ligne2))
	
	def dilater(self, coeff):
		for i in range(self.parent.largeur):
			self.parent.raw[self.index*self.parent.largeur+i] *= coeff
		# On signale que deux changements ont été effectués
		self.parent.dirty.append((self.parent.LIGNE, self.index))
		
	def transvecter(self, ligne_index, coeff):
		ligne = Ligne(self.parent, ligne_index)
		for i in range(self.parent.largeur):
			self.parent.raw[self.index*self.parent.largeur+i] += ligne[i] * coeff
		# On signale que deux changements ont été effectués
		self.parent.dirty.append((self.parent.LIGNE, self.index))
		
		
class Colonne:
	
	def __init__(self, parent, index):
		self.parent = parent
		self.index = index
	
	def __len__(self):
		return self.parent.hauteur
	
	def __getitem__(self, key):
		return self.parent[self.index, key, 'c']
		
	def __setitem__(self, key, value):
		self.parent[self.index, key, 'c'] = value
		
	def __repr__(self):
		return str(self.parent.colonnes[self.index])
		
	def echanger(self, colonne):
		colonne1, colonne2 = self.index, colonne
		
		for i in range(self.parent.hauteur):
			self.parent.raw[i*self.parent.largeur+colonne1], self.parent.raw[i*self.parent.largeur+colonne2] = self.parent.raw[i*self.parent.largeur+colonne2], self.parent.raw[i*self.parent.largeur+colonne1]
		# On signale que deux changements ont été effectués
		self.parent.dirty.append((self.parent.COLONNE, colonne1))
		self.parent.dirty.append((self.parent.COLONNE, colonne2))
			
	def dilater(self, coeff):
		
		for i in range(self.parent.hauteur):
			self.parent.raw[i*self.parent.largeur+self.index] *= coeff
		# On signale que deux changements ont été effectués
		self.parent.dirty.append((self.parent.COLONNE, self.index))	
	
	
	def transvecter(self, colonne_index, coeff):
			colonne = Colonne(self.parent, colonne_index)
			for i in range(self.parent.hauteur):
				self.parent.raw[i*self.parent.largeur+self.index] += colonne[i] * coeff
			# On signale que deux changements ont été effectués
			self.parent.dirty.append((self.parent.LIGNE, self.index))
		

class Matrice:
	
	LIGNE = 1
	COLONNE = 2
	ELEMENT = 3
	
	def __init__(self, hauteur, largeur, remplissage=lambda double_index:0, snapshot_auto=True):
		self.hauteur = hauteur
		self.largeur = largeur
		self.raw = [] # seule véritable stockage des données ligne après ligne
		
		self.snapshot_auto = snapshot_auto
		self.dirty = [] # liste de tuples (TYPE, INDEX) des éléments à mettre à jour
		self.colonnes_snapshot = [] # stocke chaque colonnes sous la forme d'un tableau
		self.lignes_snapshot = [] # stocke chaque ligne sous la forme d'un tableau
		
		# On remplit le tableau raw (les lignes se suivent)
		for i in range(len(self)):
			ligne, colonne = self.double_index(i)
			self.raw.append(remplissage((ligne, colonne)))
				
		if self.snapshot_auto:
			self.snap(force=True)
			
	def load(self, data):
		self.raw = data
		self.snap(force=True)

	def snap(self, force=False):
		"""
			Permet de créer deux images de la matrice, une classée par lignes, l'autre classée par colonnes
			Effectue automatiquement les corrections nécessaires sur les snapshots suite à un changement
		"""
		if len(self.dirty) > 0:
			for task in self.dirty:
				_type = task[0]
				_index = task[1]
				if _type == self.LIGNE:
					for i in range(self.largeur):
						# Mise à jour de ligne_snapshot
						self.lignes_snapshot[_index][i] = self[_index, i]
						# Mise à jour de colonne_snapshot
						self.colonnes_snapshot[i][_index] = self[_index, i]
				if _type == self.COLONNE:
					for i in range(self.hauteur):
						# Mise à jour de colonne_snapshot
						self.colonnes_snapshot[_index][i] = self[i, _index]
						# Mise à jour de ligne_snapshot
						self.lignes_snapshot[i][_index] = self[i, _index]
				if _type == self.ELEMENT:
					ligne, colonne = _index
					self.colonnes_snapshot[colonne][ligne] = self[ligne, colonne]
					self.lignes_snapshot[ligne][colonne] = self[ligne, colonne]
			self.dirty.clear() # Tous les élements ont été mis à jour
		elif force:
			self.lignes_snapshot = self._gen_lignes()
			self.colonnes_snapshot = self._gen_colonne()
			
	def _gen_lignes(self):
		lignes = []
		for i in range(self.hauteur):
			lignes.append(self.raw[i*self.largeur:(i + 1) * self.largeur]) 
		return lignes
		
	def _gen_colonne(self):
		colonnes = []
		for i in range(len(self)):
			ligne, colonne = self.double_index(i)
			if len(colonnes) <= colonne:
				colonnes.append([])
			colonnes[colonne].append(self[ligne, colonne])
		return colonnes
			
	def _update_raw(self):
		self.raw.clear()
		for i in range(len(self)):
			ligne, colonne = self.double_index(i)
			self.raw.append(self[colonne, ligne, 'c'])

	def __len__(self):
		return self.hauteur * self.largeur
	
	def single_index(self, ligne, colonne):
		return ligne + colonne
	
	def double_index(self, single_index):
		return (single_index//self.largeur, single_index % self.largeur)
	
	def double_index_c(self, single_index):
		return (single_index//self.hauteur, single_index % self.hauteur)
	
	def ligne(self, index):
		return Ligne(self, index)
		
	def colonne(self, index):
		return Colonne(self, index)
	
	@property
	def lignes(self):
		self.snap()
		return self.lignes_snapshot
	
	@property
	def colonnes(self):
		self.snap()
		return self.colonnes_snapshot
		
	def __getitem__(self, key):
		"""
			key = (ligne, colonne) | (colonne, ligne, 'c')
		"""

		if len(key) == 3 and key[2] == 'c':
			colonne = key[0]
			ligne = key[1]
		else:
			ligne = key[0]
			colonne = key[1]
		
		return self.raw[ligne*self.largeur+colonne]
		
	def __setitem__(self, key, value):
		"""
			key = (ligne, colonne) | (colonne, ligne, 'c')
		"""
		
		if len(key) == 3 and key[2] == 'c':
			colonne = key[0]
			ligne = key[1]
		else:
			ligne = key[0]
			colonne = key[1]
			
		self.raw[ligne*self.largeur+colonne] = value
		# On signale qu'un changement a été effectué
		self.dirty.append((self.ELEMENT, (ligne, colonne)))
			
		
		
	def __repr__(self):
		chaine = ""
		for i in range(len(self)):
			ligne, colonne = self.double_index(i)
			chaine += str(self.raw[i])
			if colonne == self.largeur-1:
				chaine += "\n"
			else:
				chaine += "\t"
		return chaine
		
	def transpose(self):
		return Matrice(self.largeur, self.hauteur, remplissage=lambda x:self[x[1], x[0]], snapshot_auto=self.snapshot_auto)
		
	def copy(self):
		return Matrice(self.hauteur, self.largeur, lambda x:self[x[0], x[1]], snapshot_auto=self.snapshot_auto)
		
	def __add__(self, M):
		return Matrice(self.hauteur, self.largeur, lambda x:self[x[0], x[1]]+M[x[0], x[1]], snapshot_auto=self.snapshot_auto)
	
	def  __mul__(self, M):
		def remplissage(double_index):
			ligne, colonne = double_index
			valeur = 0
			for i in range(self.hauteur):
				valeur += self[ligne, i] * M[i, colonne]
			return valeur
		return Matrice(self.hauteur, self.largeur, remplissage=remplissage, snapshot_auto=self.snapshot_auto)
	
class MatriceIdendite(Matrice):
	
	def __init__(self, taille):
		def remplissage(x):
			if x[0] == x[1]:
				return 1
			return 0
		super().__init__(taille, taille, remplissage)