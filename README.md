# Matrice et Pivot de Gauss

Petite implémentation des matrices et du pivot de Gauss en Python

## Matrice

### Création

Une matrice `M` se créee de la façon suivante :

```python
    M = Matrice(hauteur, largeur, [remplissage])
```

L'argument optionnel `remplissage` doit être une fonction prenant en paramètre un tuple : (ligne, colonne)

Par exemple :

```python
    def f(x):
        return x[0]+x[1]
    
    M = Matrice(4, 4, f)
    
    print(M)
```

Donne :
```
0	1	2	3
1	2	3	4
2	3	4	5
3	4	5	6
```

Si l'argument remplissage n'est pas spécifié, la matrice créée est nulle

### Utilisation

#### Manipulations internes

Il est possible de récupérer l'élément de position ligne=x, colonne=y de la manière suivante : `M[x, y]`
Il est aussi possible de récupérer le même élement de cette façon : `M[y, x, 'c']`

Plusieurs représentation de la matrice sont disponibles :
- sous forme d'un tableau de sous-tableaux, chacun représentant une ligne : `M.lignes`
- sous forme d'un tableau de sous-tableaux, chacun représentant une colonne : `M.colonnes`

Il est possible de récupérer une ligne ou une colonne d'index `x` de la manière suivante :
```python
    ligne = M.ligne(x)
    colonne = M.colonne(x)
    
    ligne[0] # accès au premier élement de la ligne
    colonne[0] accès au premier élement de la colonne
```

Il est possible d'effectuer différentes opérations sur les lignes et les colonnes :
- échanger la ligne/colonne `x` et la ligne/colonne `y` : `M.ligne(x).echanger(y)`
- dilater d'un coefficient `a` la ligne/colonne `x` : `M.colonne(x).dilater(a)`
- effectuer une tranvection de coefficient `a` de la ligne/colonne `x` sur la ligne/colonne `y` : `M.ligne(y).transvecter(x, a)`

#### Matrice transposée
`M.transpose()` renvoie la transposée de la matrice M

#### Opérations

Les opérateurs d'addition et de multiplication de python sont directement implémenter dans les matrices.
Ainsi pour additionner/multiplier deux matrices `M1` et `M2` il suffit de faire :
```python
    M3 = M1 + M2
    M4 = M1 * M2
```

## Pivot de Gauss

La classe `Gauss` représente un système linéaire et propose les outils pour le résoudre en utilisant l'algorithme du [pivot de Gauss](https://fr.wikipedia.org/wiki/Élimination_de_Gauss-Jordan)

La classe `Gauss` se comporte de la *même manière qu'une matrice*
Ainsi il est possible d'initialiser un objet `Gauss` de la façon suivante :
```python
    G = Gauss(largeur, hauteur, [remplissage])
    
    G.solve() # Lance la résolution du système
    
    print(G)
```

Il est possible de se baser sur une matrice indépendante :
```python
    M = Matrice(largeur, hauteur, [remplissage])
    
    G.solve(M)
    
    print(M)
```

La matrice est `M` est modifiée par l'objet `Gauss`
